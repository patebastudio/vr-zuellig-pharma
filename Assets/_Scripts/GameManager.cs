using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class QuizDetail
{
    public Patient patientType;
    public bool isOver;
}

[Serializable]
public class MultipleChoiceData
{
    public int scoreIfTrue;
    public bool answerKey;
}

[Serializable]
public class CheckBoxData
{
    public GameObject checkedSprite;
    public bool answerKey;
}

public enum Patient
{
    MrTH,
    MrsRI,
    MrsYU,
    Unknown
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Patient quizIndexForTesting;
    public Patient quizIndex;
    public bool isCountdown;

    [Header("Audios")]
    public AudioSource audioSource;
    public AudioClip correctAnswerClip;
    public AudioClip winnerClip;
    public AudioClip loseClip;

    [Header("Panels & Text")]
    public GameObject gameOverPanel;
    public GameObject finishPanelWithFault;
    public GameObject finishPanel;
    public GameObject nextPanel;
    public TextMeshProUGUI scoreHUDText;
    public TextMeshProUGUI timerHUDText;
    public List<GameObject> patientQuizParent;
    public List<GameObject> healthIcon;

    private void Awake()
    {
        instance = this;
        StartPatientQuiz();
    }

    public void StartPatientQuiz()
    {
        if (DataManager.instance == null) quizIndex = (Patient)quizIndexForTesting;
        else quizIndex = (Patient)(int)DataManager.instance.patient;

        isCountdown = true;
        patientQuizParent[(int)quizIndex].SetActive(true);
        scoreHUDText.text = $"{DataManager.instance.currPlayerData.score} Pt";

        StartCoroutine(CountdownTimer());
        UpdateHealthIcon();
    }

    public void UpdateHealthIcon()
    {
        for (int i = 0; i < healthIcon.Count; i++)
        {
            healthIcon[i].SetActive(false);
        }

        for (int i = 0; i < DataManager.instance.currPlayerData.health; i++)
        {
            healthIcon[i].SetActive(true);
        }
    }

    public void SetOverQuizType(Patient pat, bool cond)
    {
        isCountdown = false;
        DataManager.instance.quizDetails[(int)pat].isOver = true;

        if (!cond)
        {
            PlayNotifClip(loseClip);
            gameOverPanel.SetActive(true);
            DataManager.instance.currPlayerData.health--;
            DataManager.instance.theresFault = true;

            if (DataManager.instance.currPlayerData.timer <= 0) gameOverPanel.transform.GetChild(2).gameObject.SetActive(true);
            else gameOverPanel.transform.GetChild(1).gameObject.SetActive(true);
        }
        else
        {
            PlayNotifClip(winnerClip);
            nextPanel.SetActive(true);
        }

        UpdateHealthIcon();
        patientQuizParent[(int)quizIndex].SetActive(false);
        quizIndex = Patient.Unknown;
    }

    public void CheckAfterPanel()
    {
        bool isDone = true;
        foreach (QuizDetail quiz in DataManager.instance.quizDetails)
        {
            if (!quiz.isOver)
            {
                isDone = false;
                break;
            }
        }

        gameOverPanel.SetActive(false);
        finishPanelWithFault.SetActive(false);
        finishPanel.SetActive(false);
        nextPanel.SetActive(false);
        foreach (GameObject obj in patientQuizParent)
        {
            obj.SetActive(false);
        }

        if (!isDone)
        {
            DataManager.instance.isFirstOpen = false;
            MoveToLobby();
        }
        else
        {
            PlayNotifClip(winnerClip);
            DataManager.instance.SaveNewLeaderBoard();

            if (!DataManager.instance.theresFault) finishPanel.SetActive(true);
            else finishPanelWithFault.SetActive(true);
            
            DataManager.instance.isFirstOpen = true;
            DataManager.instance.theresFault = false;
            foreach (QuizDetail quiz in DataManager.instance.quizDetails)
            {
                quiz.isOver = false;
            }
        }

        quizIndex = Patient.Unknown;
    }

    public void MoveToLobby()
    {
        SceneManager.LoadScene("Lobby");
    }

    public void PlayNotifClip(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }

    public void SetScoring(int factor)
    {
        DataManager.instance.currPlayerData.score += factor;
        scoreHUDText.text = $"{DataManager.instance.currPlayerData.score} Pt";
    }

    public IEnumerator CountdownTimer()
    {
        DataManager.instance.currPlayerData.timer--;
        if (DataManager.instance.currPlayerData.timer <= 0)
        {
            SetOverQuizType(quizIndex, false);
        }

        yield return new WaitForSeconds(1f);

        int minutes = DataManager.instance.currPlayerData.timer / 60;
        int seconds = DataManager.instance.currPlayerData.timer % 60;
        timerHUDText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        if (isCountdown) StartCoroutine(CountdownTimer());
    }
}
