using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public string name;
    public int health;
    public int score;
    public int timer;
}

[Serializable]
public class LeaderBoardData
{
    public List<PlayerData> playerData;
}

public class DataManager : MonoBehaviour
{
    public static DataManager instance;

    public Patient patient;
    public PlayerData currPlayerData;
    public List<QuizDetail> quizDetails;

    [Header("Default Datas")]
    public int healthDefault;
    public int timerDefault;

    [Header("Temporary Datas")]
    public bool isFirstOpen;
    public bool theresFault;
    public LeaderBoardData leaderBoardData;

    private void Start()
    {
        if (instance == null)
        {
            instance = this; // In first scene, make us the singleton.
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
            Destroy(gameObject); // On reload, singleton already set, so destroy duplicate.
    }

    public IEnumerator SetupLeaderBoard(Action<LeaderBoardData> data)
    {
        bool process;
        string[] paths = { Application.persistentDataPath, "LeaderBoardData.json" };
        string leaderBoardPath = PathCombiner(paths);

        if (!File.Exists(leaderBoardPath))
        {
            string json = JsonUtility.ToJson(leaderBoardData);
            File.WriteAllText(leaderBoardPath, json);
            process = true;
        }
        else
        {
            string json = File.ReadAllText(leaderBoardPath);
            leaderBoardData = JsonUtility.FromJson<LeaderBoardData>(json);
            process = true;
        }

        yield return new WaitUntil(() => process);
        data(leaderBoardData);
    }

    public void SaveNewLeaderBoard()
    {
        StartCoroutine(SetupLeaderBoard(res =>
        {
            leaderBoardData.playerData.Add(currPlayerData);
            leaderBoardData.playerData = leaderBoardData.playerData.OrderByDescending(x => x.score).ThenBy(x => x.timer).ToList();
            leaderBoardData.playerData.RemoveAt(leaderBoardData.playerData.Count - 1);

            string[] paths = { Application.persistentDataPath, "LeaderBoardData.json" };
            string leaderBoardPath = PathCombiner(paths);
            string json = JsonUtility.ToJson(leaderBoardData);
            File.WriteAllText(leaderBoardPath, json);
            Debug.Log("Leaderboard Updated!");

            currPlayerData.name = "Player";
            currPlayerData.health = healthDefault;
            currPlayerData.timer = timerDefault;
            currPlayerData.score = 0;
        }));
    }

    string PathCombiner(string[] strings)
    {
        string newStr = string.Empty;
        for (int i = 0; i < strings.Length; i++)
        {
            newStr += $"{strings[i]}";
            if (i != strings.Length - 1)
            {
                newStr += @"/";
            }
        }

        return newStr;
    }
}
