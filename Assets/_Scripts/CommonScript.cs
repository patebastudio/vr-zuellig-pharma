using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CommonScript : MonoBehaviour
{
    public static CommonScript Instance;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        Instance = this;
    }

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void Quit()
    {
        Application.Quit();
    }
}