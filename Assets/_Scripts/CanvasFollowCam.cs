using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFollowCam : MonoBehaviour
{
    public Transform target;
    public float followSpeed = 5f;
    public float distanceFromCamera = 1f;

    private Camera mainCamera;
    private Transform canvasTransform;
    private Vector3 targetPosition;

    private void Start()
    {
        mainCamera = Camera.main;
        target = mainCamera.transform;
        canvasTransform = GetComponent<RectTransform>();
    }

    private void LateUpdate()
    {
        if (target == null)
            return;

        // Calculate the target position based on the target object's position and distance from the camera
        targetPosition = target.position - mainCamera.transform.forward * distanceFromCamera;

        // Smoothly interpolate the canvas position towards the target
        canvasTransform.position = Vector3.Lerp(canvasTransform.position, targetPosition, followSpeed * Time.deltaTime);

        // Make the canvas face the camera
        canvasTransform.LookAt(canvasTransform.position + mainCamera.transform.rotation * Vector3.forward, mainCamera.transform.rotation * Vector3.up);
    }
}
