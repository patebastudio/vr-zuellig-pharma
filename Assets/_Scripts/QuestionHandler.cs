using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class LineDetail
{
    public GameObject line;
    public bool answerKey;
}

public class QuestionHandler : MonoBehaviour
{
    public Patient quizIndex;
    public bool lastPanel;

    [Header("Multiple Choice Type")]
    public List<MultipleChoiceData> multipleChoiceDatas;

    [Header("Check Box Type")]
    public int checkBoxScore;
    public List<CheckBoxData> checkBoxDatas;

    [Header("Matching Type")]
    public int currLeftChosen;
    public int currRightChosen;
    public List<Button> leftButtons;
    public List<Button> rightButtons;
    public List<LineDetail> lineOfA;
    public List<LineDetail> lineOfB;
    public List<LineDetail> lineOfC;

    public void AnswerMultipleChoice(int index)
    {
        if (multipleChoiceDatas[index].answerKey)
        {
            GameManager.instance.SetScoring(multipleChoiceDatas[index].scoreIfTrue);

            if (lastPanel)
                GameManager.instance.SetOverQuizType(quizIndex, true);
            else
            {
                GameManager.instance.PlayNotifClip(GameManager.instance.correctAnswerClip);
                PanelManager.instance.NextPanel();
            }
        }
        else
        {
            GameManager.instance.SetOverQuizType(quizIndex, false);
        }
    }

    public void SetStateCheckBox(GameObject checkedSprite)
    {
        if (checkedSprite.activeSelf) checkedSprite.SetActive(false);
        else checkedSprite.SetActive(true);
    }

    public void AnswerCheckBox()
    {
        bool isTrue = true;
        foreach (CheckBoxData datas in checkBoxDatas)
        {
            if (datas.checkedSprite.activeSelf != datas.answerKey)
            {
                isTrue = false; 
                break;
            }
        }

        if (isTrue)
        {
            GameManager.instance.SetScoring(checkBoxScore);

            if (lastPanel)
                GameManager.instance.SetOverQuizType(quizIndex, true);
            else
            {
                GameManager.instance.PlayNotifClip(GameManager.instance.correctAnswerClip);
                PanelManager.instance.NextPanel();
            }
        }
        else
        {
            GameManager.instance.SetOverQuizType(quizIndex, false);
        }
    }

    public void ChooseLeft(int index)
    {
        currLeftChosen = index;
        leftButtons[currLeftChosen - 1].enabled = false;
        foreach (Button btn in leftButtons) btn.interactable = false;
        foreach (Button btn in rightButtons) btn.interactable = true;
    }

    public void ChooseRight(int index)
    {
        currRightChosen = index;
        rightButtons[currRightChosen - 1].enabled = false;
        foreach (Button btn in rightButtons) btn.interactable = false;
        foreach (Button btn in leftButtons) btn.interactable = true;

        SetLineMatching();
        currLeftChosen = 0;
        currRightChosen = 0;
    }

    public void SetLineMatching()
    {
        switch (currLeftChosen)
        {
            case 1:
                lineOfA[currRightChosen - 1].line.SetActive(true);
                break;

            case 2:
                lineOfB[currRightChosen - 1].line.SetActive(true);
                break;

            case 3:
                lineOfC[currRightChosen - 1].line.SetActive(true);
                break;
        }
    }

    public void ResetMatching()
    {
        foreach (LineDetail line in lineOfA) line.line.SetActive(false);
        foreach (LineDetail line in lineOfB) line.line.SetActive(false);
        foreach (LineDetail line in lineOfC) line.line.SetActive(false);

        foreach (Button btn in leftButtons) btn.enabled = true;
        foreach (Button btn in rightButtons) btn.enabled = true;
    }

    public bool MatchingLineChecker(List<LineDetail> line)
    {
        bool isTrue = true;
        foreach (LineDetail currLine in line)
        {
            if (currLine.line.activeSelf && !currLine.answerKey ||
                !currLine.line.activeSelf && currLine.answerKey)
            {
                isTrue = false;
                break;
            }
        }

        return isTrue;
    }

    public void SubmitMatching()
    {
        bool[] isTrue = {
        MatchingLineChecker(lineOfA),
        MatchingLineChecker(lineOfB),
        MatchingLineChecker(lineOfC)
        };

        bool allTrue = true;
        foreach (bool temp in isTrue)
        {
            if (!temp)
            {
                allTrue= false;
                break;
            }
        }

        if (allTrue)
        {
            GameManager.instance.SetScoring(checkBoxScore);

            if (lastPanel)
                GameManager.instance.SetOverQuizType(quizIndex, true);
            else
            {
                GameManager.instance.PlayNotifClip(GameManager.instance.correctAnswerClip);
                PanelManager.instance.NextPanel();
            }
        }
        else
        {
            GameManager.instance.SetOverQuizType(quizIndex, false);
        }
    }
}
