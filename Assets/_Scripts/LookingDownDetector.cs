using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LookingDownDetector : MonoBehaviour
{
    public float lookingDownBoundary;
    public string rot;
    public string localRot;
    public UnityEvent whenLookingDown;
    public UnityEvent whenNotLookingDown;

    void Update()
    {
        rot = transform.rotation.ToString();
        localRot = transform.localRotation.ToString();
        if (transform.rotation.x <= lookingDownBoundary)
        {
            whenLookingDown.Invoke();
        }
        else
        {
            whenNotLookingDown.Invoke();
        }
    }
}
