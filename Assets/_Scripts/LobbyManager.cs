using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LobbyManager : MonoBehaviour
{
    [Header("Addon For SpawnPoint")]
    public Transform xrOrigin;
    public Transform firstSpawnPoint;
    public Transform afterGameSpawnPoint;
    public List<GameObject> inactiveObjWhenAfterGame;
    public List<GameObject> parentPatientButton;

    [Header("Addon For Input Name")]
    public Button nextButton;
    public TMP_InputField inputField;

    [Header("Temporary Datas")]
    public List<Transform> leaderBoardList;

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject obj in inactiveObjWhenAfterGame)
            obj.SetActive(DataManager.instance.isFirstOpen);

        switch (DataManager.instance.isFirstOpen)
        {
            case true:
                xrOrigin.position = firstSpawnPoint.position;
                break;

            case false:
                xrOrigin.position = afterGameSpawnPoint.position;
                for (int i = 0; i < parentPatientButton.Count; i++)
                    if (DataManager.instance.quizDetails[i].isOver)
                        parentPatientButton[i].SetActive(false); 
                break;
        }

        StartCoroutine(DataManager.instance.SetupLeaderBoard(res =>
        {
            for (int i = 0; i < res.playerData.Count; i++)
            {
                leaderBoardList[i].gameObject.SetActive(true);
                leaderBoardList[i].GetChild(1).GetComponent<TextMeshProUGUI>().text = res.playerData[i].name;
                leaderBoardList[i].GetChild(2).GetComponent<TextMeshProUGUI>().text = res.playerData[i].score.ToString();
            }
        }));
    }

    public void EnteringLobby(string soundName)
    {
        SoundManager.instance.PlayLooping(soundName, true);
    }

    public void SetPlayerName(string name)
    {
        SoundManager.instance.PlayOnce("Click Button");
        DataManager.instance.currPlayerData.name = name;
        if (!string.IsNullOrEmpty(name)) nextButton.gameObject.SetActive(true);
        else nextButton.gameObject.SetActive(false);
    }

    public void ChangeScene(int patientSceneID)
    {
        DataManager.instance.patient = (Patient)patientSceneID;
        SceneManager.LoadScene("Poly");
    }
}
