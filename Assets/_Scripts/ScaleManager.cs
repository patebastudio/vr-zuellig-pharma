using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleManager : MonoBehaviour
{
    public float scaleMin;
    public float scaleMax;
    public Vector3 scaleFactor;
    public Button scaleUpBtn;
    public Button scaleDownBtn;
    public Vector3 defaultPos;
    public Vector3 modifiedPos;

    public void ScaleUp()
    {
        transform.localScale += scaleFactor;
        transform.localPosition = modifiedPos;

        if (transform.localScale.x < scaleMax) scaleUpBtn.interactable = true;
        else scaleUpBtn.interactable = false;

        if (transform.localScale.x > scaleMin) scaleDownBtn.interactable = true;
        else scaleDownBtn.interactable = false;
    }

    public void ScaleDown()
    {
        transform.localScale -= scaleFactor;

        if (transform.localScale.x < scaleMax) scaleUpBtn.interactable = true;
        else scaleUpBtn.interactable = false;

        if (transform.localScale.x > scaleMin) scaleDownBtn.interactable = true;
        else
        {
            scaleDownBtn.interactable = false;
            transform.localPosition = defaultPos;
        }
    }
}
