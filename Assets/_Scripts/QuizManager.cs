using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizManager : MonoBehaviour
{
    public GameObject selection, BGChar;
    public bool selectionBool, BGCharBool;

    void Start()
    {
        selectionBool = false;
        BGCharBool = true;
    }
    void Update()
    {
        if (BGCharBool)
            selectionBool = false;
        if (selectionBool)
            BGCharBool = false;

        if (BGCharBool)
        {
            selection.SetActive(false);
            BGChar.SetActive(true);
        }
        if (selectionBool)
        {
            BGChar.SetActive(false);
            selection.SetActive(true);
        }
    }

    public void onPatientSelection()
    {
        selectionBool = true;
        BGCharBool = false;
    }
}
