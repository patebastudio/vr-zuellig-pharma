using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

[Serializable]
public class QuestionPanelDetail
{
    public GameObject questionPanel;
    public List<AudioClip> audioClips;
}

public class PanelManager : MonoBehaviour
{
    public static PanelManager instance;

    public int clipIndex;
    public int panelIndex;
    public List<QuestionPanelDetail> questionPanels;

    [Header("Addon For Game Panel")]
    public bool isGame;
    public Patient quizIndex;
    public AudioSource source;
    public List<AudioClip> clips;

    [Header("Addon If Any Event After Finish")]
    public UnityEvent afterFinishAllPanel;

    private void Awake()
    {
        instance = this;
        source = gameObject.AddComponent<AudioSource>();
        source.playOnAwake = false;
        
        if (isGame)
        {
            GameManager.instance.quizIndex = quizIndex;
            CustomPanelIndex(0);
        }
    }

    public void ResetPanel()
    {
        StopCoroutine(PlayAudioClips());
        SoundManager.instance.PlayOnce("Click Button");

        foreach (QuestionPanelDetail obj in questionPanels)
        {
            obj.questionPanel.SetActive(false);
        }

        clipIndex = 0;
        clips.Clear();
        source.Stop();
    }

    public void PrevPanel()
    {
        ResetPanel();
        panelIndex--;

        questionPanels[panelIndex].questionPanel.SetActive(true);
        clips = questionPanels[panelIndex].audioClips;
        StartCoroutine(PlayAudioClips());
    }

    public void CustomPanelIndex(int index)
    {
        ResetPanel();
        panelIndex = index;

        questionPanels[panelIndex].questionPanel.SetActive(true);
        clips = questionPanels[panelIndex].audioClips;
        StartCoroutine(PlayAudioClips());
    }

    public void NextPanel()
    {
        ResetPanel();
        panelIndex++;

        if (panelIndex < questionPanels.Count)
        {
            questionPanels[panelIndex].questionPanel.SetActive(true);
            clips = questionPanels[panelIndex].audioClips;
            StartCoroutine(PlayAudioClips());
        }
        else
            afterFinishAllPanel.Invoke();
    }

    public void ShowImageOnLeftHand(Sprite sprite)
    {
        HandImagePlaceHolder.Instance.ReplaceImage(sprite, true);
    }

    public void HideImageOnLeftHand()
    {
        HandImagePlaceHolder.Instance.ReplaceImage(null, false);
    }

    IEnumerator PlayAudioClips()
    {
        if (clips.Count > 1)
        {
            source.PlayOneShot(clips[clipIndex]);

            yield return new WaitForSeconds(1f);
            yield return new WaitUntil(() => !source.isPlaying);

            if (clipIndex < clips.Count - 1) 
            {
                clipIndex++;
                StartCoroutine(PlayAudioClips());
            }
        }
        else if (clips.Count == 1)
        {
            source.PlayOneShot(clips[0]);
        }
    }
}
