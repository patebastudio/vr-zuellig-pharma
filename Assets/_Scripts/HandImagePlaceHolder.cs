using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandImagePlaceHolder : MonoBehaviour
{
    public static HandImagePlaceHolder Instance;
    public Image imagePlaceHolder;

    private void Awake()
    {
        Instance = this;
    }

    public void ReplaceImage(Sprite sprite, bool cond)
    {
        imagePlaceHolder.sprite = sprite;
        imagePlaceHolder.transform.parent.gameObject.SetActive(cond);
    }
}
