using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Preload : MonoBehaviour
{
    public float preloadDelay;
    public string firstScene;
    public List<GameObject> splashObj;

    public void Start()
    {
        for (int i = 0; i < splashObj.Count; i++)
        {
            splashObj[i].SetActive(false);
        }

#if UNITY_EDITOR
        ChangeScene(firstScene);
#endif

        StartCoroutine(GoToScene());
    }

    public IEnumerator GoToScene()
    {
        //yield return new WaitForSeconds(preloadDelay);
        for (int i = 0; i < splashObj.Count; i++)
        {
            splashObj[i].SetActive(true);
            yield return new WaitForSeconds(preloadDelay);
            splashObj[i].SetActive(false);
        }

        ChangeScene(firstScene);
    }

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
