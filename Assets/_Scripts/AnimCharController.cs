using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimCharController : MonoBehaviour
{
    public Animator animator;
    public AudioClip coughClip;
    public string defaultTrigger = "isIdle";
    public string nextTrigger = "isSick";
    public bool defaultTriggerBool;
    public bool nextTriggerBool;

    AudioSource source;

    void Awake()
    {
        if (coughClip == null) return;

        source = gameObject.AddComponent<AudioSource>();
        source.clip = coughClip;
        source.loop = true;
        source.Play();
    }

    void Update()
    {
        animator.SetBool(defaultTrigger, defaultTriggerBool);
        animator.SetBool(nextTrigger, nextTriggerBool);
        source.mute = !nextTriggerBool;
    }
}