using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SoundDetail
{
    public string soundName;
    public AudioSource audioSource;
    public AudioClip audioClip;
}

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    public SoundDetail[] soundDetails;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this; // In first scene, make us the singleton.
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
            Destroy(gameObject); // On reload, singleton already set, so destroy duplicate.

        foreach (SoundDetail sound in soundDetails)
        {
            var source = gameObject.AddComponent<AudioSource>();
            sound.audioSource = source;
            source.playOnAwake = false;
            source.clip = sound.audioClip; 
            source.volume = .5f; 
        }

        PlayLooping("Opening", true);
    }

    public void PlayLooping(string name, bool stopAll)
    {
        if (stopAll) StopAllAudio();
        int i = Array.FindIndex(soundDetails, x => x.soundName == name);
        soundDetails[i].audioSource.loop = true;
        soundDetails[i].audioSource.Play();
    }

    public void PlayOnce(string name)
    {
        int i = Array.FindIndex(soundDetails, x => x.soundName == name);
        soundDetails[i].audioSource.loop = false;
        soundDetails[i].audioSource.Play();
    }

    public void Pause(string name)
    {
        int i = Array.FindIndex(soundDetails, x => x.soundName == name);
        soundDetails[i].audioSource.Pause();
    }

    public void Stop(string name)
    {
        int i = Array.FindIndex(soundDetails, x => x.soundName == name);
        soundDetails[i].audioSource.Stop();
    }

    public void StopAllAudio()
    {
        foreach (SoundDetail sound in soundDetails)
        {
            sound.audioSource.Stop();
        }
    }
}
