using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionDetector : MonoBehaviour
{
    public string collisionTagTarget;
    public UnityEvent whenDetected;
    public UnityEvent whenNotDetected;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(collisionTagTarget))
        {
            whenDetected.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(collisionTagTarget))
        {
            whenNotDetected.Invoke();
        }
    }
}
